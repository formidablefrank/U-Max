angular.module('starter.controllers', ['ionic', 'ngAnimate', 'chart.js'])

.controller('DashCtrl', function($scope, $location) {
  $scope.invchoices = [{id: 0, name: "Choose below..."},
                       {id: 1, name: "House and/or lot"},
                       {id: 2, name: "Auto"},
                       {id: 3, name: "Portfolios"},
                       {id: 4, name: "Time Deposit"},
                       {id: 5, name: "Foreclosed Assets"}
                      ];
  $scope.invchoice = $scope.invchoices[0];
  $scope.invchange = function(item){
    switch (item.id) {
      case 1:
        $location.path('/inv/house');
        break;
      case 2:
        $location.path('/inv/car');
        break;
      case 3:
        $location.path('/inv/stock');
        break;
      case 4:
        $location.path('/inv/time');
        break;
      case 5:
        $location.path('/inv/asset');
        break;
    }
  }
})

.controller('HouseCtrl', function($scope) {
  $scope.myValues= [true,false,false,false];
  $scope.ateGurl= "img/happy.png";
  var ctr=1;
  $scope.showAlert = function(){
    for (var i=0; i <= 3; i++) {
      $scope.myValues[i]=false;
    };
    if(ctr==3){
      $scope.ateGurl= "img/vhappy.png";
    }
    $scope.myValues[ctr]=true;
    ctr+=1;
  };
  // $scope.loan = (parseFloat($scope.income) * parseFloat($scope.term) * 4.8) / 1.30231514285714;
  $scope.loan = 7142158.67;
})

.controller('CarCtrl', function($scope) {
  $scope.myValues = [true,false,false,false];
  $scope.ateGurl= "img/happy.png";
  var ctr=1;
  $scope.showAlert = function(){
    for (var i=0; i <= 8; i++) {
      $scope.myValues[i]=false;
    };
    if(ctr==1) {
      if(parseInt($scope.carloan) >= 200000) ctr++;
    }
    if(ctr==2){
      $scope.ateGurl= "img/sad.png";
    }
    $scope.myValues[ctr]=true;
    ctr+=1;
  };

  $scope.carchoices = [{id: 0, name: "Click here..."},
                       {id: 1, name: "Audi"},
                       {id: 2, name: "Porshe"},
                       {id: 3, name: "Toyota"}
                      ];
  $scope.carchoice = $scope.carchoices[0];

  $scope.modelchoices = [{id: 0, name: "Click here..."},
                       {id: 1, name: "Audi"},
                       {id: 2, name: "Porshe"},
                       {id: 3, name: "Toyota"}
                      ];
  $scope.modelchoice = $scope.modelchoices[0];

  $scope.modelchoices = [{id: 0, name: "Click here..."},
                       {id: 1, name: "Audi"},
                       {id: 2, name: "Porshe"},
                       {id: 3, name: "Toyota"}
                      ];
  $scope.modelchoice = $scope.modelchoices[0];


  $scope.downchoices = [{id: 0, name: "Click here..."},
                       {id: 1, name: "30%"},
                       {id: 2, name: "40%"},
                       {id: 3, name: "50%"},
                       {id: 4, name: "60%"}
                      ];
  $scope.downchoice = $scope.downchoices[0];

  $scope.cartermchoices = [{id: 0, name: "Click here..."},
                       {id: 1, name: "30%"},
                       {id: 2, name: "40%"},
                       {id: 3, name: "50%"},
                       {id: 4, name: "60%"}
                      ];
  $scope.cartermchoice = $scope.cartermchoices[0];
})


.controller('TimeCtrl', function($scope) {
  $scope.ateGurl= "img/happy.png";

  $scope.timechoices = [{id: 0, name: "Click here..."},
                       {id: 1, name: "12"},
                       {id: 2, name: "18"},
                       {id: 3, name: "24"},
                       {id: 4, name: "30"}
                      ];
  $scope.timechoice = $scope.timechoices[0];
  $scope.myValues= [true,false,false,false]
  ctr=1;
  $scope.showAlert = function(){
    for (var i=0; i <= 3; i++) {
      $scope.myValues[i]=false;
    };
    $scope.myValues[ctr]=true;
  if(ctr==3){
      $scope.ateGurl= "img/vhappy.png";
  }
    ctr+=1;
  };
})


.controller('StockCtrl', function($scope) {
  $scope.ateGurl= "img/happy.png";

  $scope.currchoices = [{id: 0, name: "Click here..."},
                       {id: 1, name: "Peso"},
                       {id: 2, name: "Dollar"}
                      ];
  $scope.currchoice = $scope.currchoices[0];

  $scope.minchoices = [{id: 0, name: "Click here..."},
                       {id: 1, name: "Php50,000"},
                       {id: 2, name: "Php100,000"}
                      ];
  $scope.minchoice = $scope.minchoices[0];

  $scope.trustchoices = [{id: 0, name: "Click here..."},
                       {id: 1, name: "1%"},
                       {id: 2, name: "2%"}
                      ];
  $scope.trustchoice = $scope.trustchoices[0];


  $scope.riskchoices = [{id: 0, name: "Click here..."},
                       {id: 1, name: "Low"},
                       {id: 2, name: "Moderate"},
                       {id: 2, name: "High"}
                      ];
  $scope.riskchoice = $scope.riskchoices[0];
  $scope.myValues= [true,false,false,false,false,false]
  ctr=1;
  $scope.showAlert = function(){
    for (var i=0; i <= 5; i++) {
      $scope.myValues[i]=false;
    };
    if(ctr==2){
        $scope.ateGurl= "img/vhappy.png";
    }
    if(ctr==5){
        $scope.ateGurl= "img/vhappy.png";
    }
    $scope.myValues[ctr]=true;
    ctr+=1;

  };

})

.controller('AssetCtrl', function($scope, Assets) {
  $scope.assets = Assets.all();
  $scope.myValues= [true,false,false]
  $scope.ateGurl= "img/happy.png";

  ctr=1;
  $scope.showAlert = function(){
    for (var i=0; i <= 3; i++) {
      $scope.myValues[i]=false;
    };
    $scope.myValues[ctr]=true;
    if(ctr==5){
      $scope.myValues[6]=true;
    }
    if(ctr==2){
        $scope.ateGurl= "img/vhappy.png";
    }
    if(ctr==6){
        $scope.ateGurl= "img/vhappy.png";
    }
    ctr+=1;
  };
    $scope.assetchoices = [{id: 0, name: "Click here..."},
                       {id: 1, name: "Agricultural Land"},
                       {id: 2, name: "Commercial Space"},
                       {id: 3, name: "Industrial Space"},
                       {id: 4, name: "Residential Space"},
                       {id: 5, name: "Pre-owned vehicle"}
                      ];
  $scope.assetchoice = $scope.assetchoices[0];
})

.controller('AssetViewCtrl', function($scope, Assets, $stateParams) {
  $scope.asset = Assets.get($stateParams.assetid);
})

.controller('AcctCtrl', function($scope, $location) {
  $scope.actchoices = [{id: 0, name: "Click here..."},
                       {id: 1, name: "Check balance and expenses"},
                       {id: 2, name: "Transfer funds"},
                       {id: 3, name: "Buy loads"},
                       {id: 4, name: "Pay bills"},
                       {id: 5, name: "Open new account"}
                      ];
  $scope.actchoice = $scope.actchoices[0];
  $scope.actchange = function(item){
    switch (item.id) {
      case 1:
        $location.path('/acc/savings');
        break;
      case 2:
        $location.path('/acc/transfer');
        break;
      case 3:
        $location.path('/acc/buyload');
        break;
      case 4:
        $location.path('/acc/paybills');
        break;
      case 5:
        $location.path('/acc/new');
        break;
    }
  }
})

.controller('SavingsCtrl', function($scope){
  $scope.labels = ["January", "February", "March", "April", "May", "June", "July"];
  $scope.series = ['Savings', 'Expenses'];
  $scope.data = [
    [65, 59, 80, 81, 56, 55, 40],
    [28, 48, 40, 19, 86, 27, 90]
  ];
  $scope.onClick = function (points, evt) {
    console.log(points, evt);
  };
  $scope.colors = ['#45b7cd', '#ff6384'];
  $scope.datasetOverride = [{ yAxisID: 'y-axis-1' }];
  $scope.options = {
    scales: {
      yAxes: [
        {
          id: 'y-axis-1',
          type: 'linear',
          display: true,
          position: 'left'
        }
      ]
    }
  };
})

.controller('MapCtrl', function($scope, $ionicLoading, $compile) {
    var myLatlng = new google.maps.LatLng(14.584981,121.0615894);

    var mapOptions = {
      center: myLatlng,
      zoom: 16,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("map"), mapOptions);

    //Marker + infowindow + angularjs compiled ng-click
    var contentString = "Your current location";

    var infowindow = new google.maps.InfoWindow({
      content: contentString
    });

    var marker = new google.maps.Marker({
      position: myLatlng,
      map: map
    });

    google.maps.event.addListener(marker, 'click', function() {
      infowindow.open(map,marker);
    });

    var service = new google.maps.places.PlacesService(map);
    service.nearbySearch({
      location: myLatlng,
      radius: 500,
      name: ['unionbank of the philippines'],
    }, callback);

    var callback = function(results, status) {
      if (status === google.maps.places.PlacesServiceStatus.OK) {
        for (var i = 0; i < results.length; i++) {
          createMarker(results[i]);
        }
      }
    }

    var createMarker = function(place) {
      var placeLoc = place.geometry.location;
      var marker = new google.maps.Marker({
        map: map,
        position: place.geometry.location,
      });

      google.maps.event.addListener(marker, 'click', function() {
        infowindow.setContent(place.name);
        infowindow.open(map, this);
      });
    }

    navigator.geolocation.getCurrentPosition(function(pos) {
      map.setCenter(new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude));
    }, function(error) {
      alert('Unable to get location: ' + error.message);
    });
})

.controller('ContactCtrl', function($scope) {
  $scope.contactchoices = [{id: 0, name: "Click here..."},
                       {id: 1, name: "Call Us"},
                       {id: 2, name: "Text Us"},
                       {id: 3, name: "Email Us"},
                       {id: 4, name: "Branch Locator"},
                      ];
  $scope.contactchoice = $scope.contactchoices[0];
  $scope.conchange = function(item, $location, $window){
    switch (item.id) {
      case 1:
        window.location.href('tel:+1-1800-555-5555');
        break;
      case 2:
        $location.path('/inv/car');
        break;
      case 3:
        $location.path('/inv/stock');
        break;
      case 4:
        $location.path('/inv/time');
        break;
    }
  }
});
