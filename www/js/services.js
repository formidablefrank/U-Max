angular.module('starter.services', [])

.factory('Chats', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var chats = [{
    id: 0,
    name: 'Ben Sparrow',
    lastText: 'You on your way?',
    face: 'img/ben.png'
  }, {
    id: 1,
    name: 'Max Lynx',
    lastText: 'Hey, it\'s me',
    face: 'img/max.png'
  }, {
    id: 2,
    name: 'Adam Bradleyson',
    lastText: 'I should buy a boat',
    face: 'img/adam.jpg'
  }, {
    id: 3,
    name: 'Perry Governor',
    lastText: 'Look at my mukluks!',
    face: 'img/perry.png'
  }, {
    id: 4,
    name: 'Mike Harrington',
    lastText: 'This is wicked good ice cream.',
    face: 'img/mike.png'
  }];

  return {
    all: function() {
      return chats;
    },
    remove: function(chat) {
      chats.splice(chats.indexOf(chat), 1);
    },
    get: function(chatId) {
      for (var i = 0; i < chats.length; i++) {
        if (chats[i].id === parseInt(chatId)) {
          return chats[i];
        }
      }
      return null;
    }
  };
})

.factory('Assets', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var assets = [{
    id: 0,
    name: 'Forbes Village',
    address: 'Makati City',
    image: 'img/foreclosedBG.png',
    price: 1250000.00
  }, {
    id: 1,
    name: 'Addition Hills',
    address: 'Mandaluyong City',
    image: 'img/foreclosedBG.png',
    price: 250000.00
  }, {
    id: 2,
    name: 'La Vista Executive Village',
    address: 'Quezon City',
    image: 'img/foreclosedBG.png',
    price: 750000.00
  }, {
    id: 3,
    name: 'Loyola Heights',
    address: 'Quezon City',
    image: 'img/foreclosedBG.png',
    price: 900000.00
  }, {
    id: 4,
    name: 'Alabang South Village',
    address: 'Las Pinas City',
    image: 'img/foreclosedBG.png',
    price: 350000.00
  }];

  return {
    all: function() {
      return assets;
    },
    remove: function(asset) {
      assets.splice(assets.indexOf(asset), 1);
    },
    get: function(assetId) {
      for (var i = 0; i < assets.length; i++) {
        if (assets[i].id === parseInt(assetId)) {
          return assets[i];
        }
      }
      return null;
    }
  };
});
