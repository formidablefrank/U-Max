# U:Max

U:Max is a virtual financial adviser (implemented in both web and mobile) that offers Unionbank services that suits their needs. It can help clients to find the right loan, invest on multiple stock portfolios or time deposit plans depending on his capacity and self-assessment. Series of questions are arranged in a systematic way with minimalistic and user-friendly interface for optimal mobile banking experience. It also proposes to improve UMobile by adding more features. With U:Max we bring Unionbank services closer to our clients anytime, anywhere. By asking the right questions, they make the best choices. This is the future of mobile banking.

**Entry to Unionbank Hackathon 2016, Aug 27-28, 2016, SM Megamall Megatrade Hall 3**

## Nature
Web app and mobile app

## Tech
- Node.js
- Ionic framework
- Angular.js
- ngCordova
- Deployed thru IBM Bluemix
- Android, iOS

## Team InnovBBz
- Joshua Frankie Rayo
- Anne Frances Calceta
- Mary Abigail Del Castillo
